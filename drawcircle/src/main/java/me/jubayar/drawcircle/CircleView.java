package me.jubayar.drawcircle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CircleView extends View {

    private int circleBackgroundColor, circleBorderColor;
    private float circleStrokeWidth;
    private int diff = 10;

    private Paint BGPaint;
    private Paint borderPaint;

    public CircleView(Context context) {
        super(context);
    }

    public CircleView(Context context, AttributeSet attrs){
        super(context, attrs);

        BGPaint = new Paint();
        borderPaint = new Paint();

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CircleView, 0, 0);

        try {
            circleBackgroundColor = a.getInteger(R.styleable.CircleView_circleBackgroundColor, 0);//0 is default
            circleBorderColor = a.getInteger(R.styleable.CircleView_circleBorderColor, 0);
            circleStrokeWidth = a.getFloat(R.styleable.CircleView_circleStrokeWidth, 3.0f);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int viewWidthHalf = this.getMeasuredWidth()/2;
        int viewHeightHalf = this.getMeasuredHeight()/2;

        int radius = 0;

        if(viewWidthHalf > viewHeightHalf)
            radius = viewHeightHalf - diff;
        else
            radius = viewWidthHalf - diff;

        BGPaint.setStyle(Paint.Style.FILL);
        BGPaint.setAntiAlias(true);
        BGPaint.setColor(circleBackgroundColor);

        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setAntiAlias(true);
        borderPaint.setColor(circleBorderColor);
        borderPaint.setStrokeWidth(circleStrokeWidth);

        canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, borderPaint);
        canvas.drawCircle(viewWidthHalf, viewHeightHalf, radius, BGPaint);
    }

    public int getCircleBackgroundColor(){
        return circleBackgroundColor;
    }

    public void setCircleBackgroundColor(int newColor){
        circleBackgroundColor = newColor;
        invalidate();
        requestLayout();
    }

    public int getCircleBorderColor(){
        return circleBorderColor;
    }

    public void setCircleBorderColor(int newColor) {
        circleBorderColor = newColor;
        invalidate();
        requestLayout();
    }

    public float getCircleStrokeWidth() {
        return circleStrokeWidth;
    }

    public void setCircleStrokeWidth(float newStrokeWidth) {
        circleStrokeWidth = newStrokeWidth;

        if (newStrokeWidth > 10) {
            diff = (int)newStrokeWidth + 5;
        } else {
            diff = 10;
        }

        invalidate();
        requestLayout();
    }
}